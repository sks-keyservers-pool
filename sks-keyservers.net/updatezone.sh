#!/bin/sh
PATH=/usr/sbin/:/usr/bin:/bin:$PATH
export PATH
date
cd /webs/sks-keyservers.net/

/usr/sbin/rndc flush 2>&1

php status/get_zonedata.php > zoneinfo.txt

if [ -n "`cat zoneinfo.txt`" ]; then
	TMPFILE=$(mktemp)
	TIMESTRING=$(m=$(date +%M); m=${m:0:1}; date +3%y%m%d%H$m)
	(cat zonetpl | sed -e "s/XXXXXXXXX/${TIMESTRING}/"; cat zoneinfo.txt;) > ${TMPFILE}
	named-checkzone sks-keyservers.net ${TMPFILE}
	if [[ $? == 0 ]]; then
		cat ${TMPFILE} > /var/bind/pri/sks-keyservers.net.zone
	else
		D=
		named-checkzone sks-keyservers.net ${TMPFILE} > /var/log/sks-keyservers.net/${TIMESTRING}_error.txt
		cat ${TMPFILE} > /var/log/sks-keyservers.net/${TIMESTRING}_zone.txt
	fi
	/usr/sbin/rndc reload 2>&1
	rm ${TMPFILE}
fi;
